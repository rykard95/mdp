Author: Richard Doan (rdoan6)

Code Repo: https://gitlab.com/rykard95/mdp

Run Command:
    `python Main.py`

Outputs into "outputs" directory
    a set of plots comparing:
        - the number of iterations to convergence
        - the time to convergence
    a json of the data used to generate the plots as well as the solution to each grid size for each method

