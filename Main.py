from mdptoolbox.mdp import PolicyIteration, ValueIteration
import numpy as np
import pandas as pd
import time
import json
import matplotlib.pyplot as plt

from GridWorld.GridModel import GridModel, QLearner

CLOCK_TIME = "clockTime"
NUM_ITERATION = "numIterations"
SOLUTION = "solution"


def getIterationStatistics(iterationMethod):
    tic = time.time()
    iterationMethod.run()
    toc = time.time()
    stats = {}
    stats[CLOCK_TIME] = toc - tic
    stats[NUM_ITERATION] = iterationMethod.iter
    stats[SOLUTION] = iterationMethod.policy
    return stats


def getConvergenceStatistics(discount=0.7, outputLocation="../outputs/vpIteration.json"):
    statistics = {}
    statistics["policyIteration"] = {}
    statistics["valueIteration"] = {}
    statistics["qLearning"] = {}
    for size in range(4, 41, 2):
        print("Generating ({}, {}) grid model".format(size, size))
        model = GridModel(size, size, [np.random.randint(0, size, size=(2,))])

        policyIteration = PolicyIteration(model.getTransitionProbs(), model.getRewards(), discount=discount)
        statistics["policyIteration"][size * size] = getIterationStatistics(policyIteration)

        valueIteration = ValueIteration(model.getTransitionProbs(), model.getRewards(), discount=discount)
        statistics["valueIteration"][size * size] = getIterationStatistics(valueIteration)

        qLearning =  QLearner(model, epochs=200, learning_rate=0.75, epsilon=0.5, discount=0.7)
        statistics["qLearning"][size * size] = getIterationStatistics(qLearning)


    with open(outputLocation, "w+") as f:
        json.dump(statistics, f, indent=4, separators=(',', ': '), sort_keys=True)

    return statistics


def plotStatistics(iterationStatisticsFilename="../outputs/vpIteration.json"):
    with open(iterationStatisticsFilename, "r") as f:
        statistics = json.load(f)

    clockTimes = []
    numIterations = []
    sizes = []
    workingStatistics = statistics["valueIteration"]
    for statistic in workingStatistics:
        sizes.append(statistic)
        clockTimes.append(workingStatistics[statistic][CLOCK_TIME])
        numIterations.append(workingStatistics[statistic][NUM_ITERATION])
    valueIterationdf = pd.DataFrame({"numStates": sizes, CLOCK_TIME: clockTimes, NUM_ITERATION: numIterations})

    clockTimes = []
    numIterations = []
    sizes = []
    workingStatistics = statistics["policyIteration"]
    for statistic in workingStatistics:
        sizes.append(statistic)
        clockTimes.append(workingStatistics[statistic][CLOCK_TIME])
        numIterations.append(workingStatistics[statistic][NUM_ITERATION])
    policyIterationdf = pd.DataFrame({"numStates": sizes, CLOCK_TIME: clockTimes, NUM_ITERATION: numIterations})

    clockTimes = []
    numIterations = []
    sizes = []
    workingStatistics = statistics["qLearning"]
    for statistic in workingStatistics:
        sizes.append(statistic)
        clockTimes.append(workingStatistics[statistic][CLOCK_TIME])
        numIterations.append(workingStatistics[statistic][NUM_ITERATION])
    qLearningdf = pd.DataFrame({"numStates": sizes, CLOCK_TIME: clockTimes, NUM_ITERATION: numIterations})

    clockTimedf = pd.DataFrame({"numStates": policyIterationdf["numStates"], "policyIterClockTime":
        policyIterationdf[CLOCK_TIME], "valueIterClockTime": valueIterationdf[CLOCK_TIME], "qLearningClockTime":
    qLearningdf[CLOCK_TIME]})
    plot = clockTimedf.plot.line()
    plt.title("Clock Time vs. Number of States Comparison")
    plt.ylabel("{}".format("Clock Time (s)"))
    plt.xlabel("Number of States")
    plt.xticks(clockTimedf.index[::5], clockTimedf["numStates"][::5])
    fig = plot.get_figure()
    fig.savefig("../outputs/clockTimeComparison.png")

    plt.clf()

    numItersdf = pd.DataFrame({"numStates": policyIterationdf["numStates"], "policyIterNumIters":
        policyIterationdf[NUM_ITERATION], "valueIterNumIters": valueIterationdf[NUM_ITERATION], "qLearningNumIters":
        qLearningdf[NUM_ITERATION]})
    plot = numItersdf.plot.line()
    plt.title("Number of Iterations vs. Number of States Comparison")
    plt.ylabel("{}".format("Number of Iterations"))
    plt.xlabel("Number of States")
    plt.xticks(numItersdf.index[::5], numItersdf["numStates"][::5])
    fig = plot.get_figure()
    fig.savefig("../outputs/numberOfIterationsComparison.png")

    plt.clf()




if __name__ == "__main__":
    getConvergenceStatistics()
    plotStatistics()
