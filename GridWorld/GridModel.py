import numpy as np
from mdptoolbox.mdp import PolicyIteration, ValueIteration

NORTH = "north"
SOUTH = "south"
EAST = "east"
WEST = "west"


class State:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "({}, {})".format(self.x, self.y)

    def __add__(self, other):
        return State(self.x + other.x, self.y + other.y)

    def distance(self, other):
        return abs(self.x - other.x) + abs(self.y - other.y)

    def correct(self, limit_x, limit_y):
        self.x = max(0, min(limit_x - 1, self.x))
        self.y = max(0, min(limit_y - 1, self.y))
        return self

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash("{}{}".format(hash(self.x), hash(self.y)))


ACTIONS = {
    NORTH: State(0, -1),
    SOUTH: State(0, 1),
    EAST: State(1, 0),
    WEST: State(-1, 0)
}


class GridModel:

    def __init__(self, height, width, goalStates, walls=[]):
        self.height = height
        self.width = width
        self.goalStates = set([State(*goalState) for goalState in goalStates])
        self.states = [State(x, y) for y in range(height) for x in range(height)]
        self.enumerated_state = {self.states[i]: i for i in range(len(self.states))}
        self.walls = {State(*wall) for wall in walls}

    def getStates(self):
        return self.states

    @property
    def numStates(self):
        return len(self.states)

    def applyAction(self, s, a):
        nextState = (s + ACTIONS[a]).correct(self.width, self.height)
        if nextState in self.walls:
            return s
        return nextState

    def getNeighbors(self, s):
        neighbors = set()
        neighbors.add(s)
        for action in ACTIONS:
            neighbors.add(self.applyAction(s, action))
        return neighbors

    def getTransitionProbs(self):
        action_matrices = []
        for action in ACTIONS:
            matrices = []
            for state in self.states:
                matrix = np.zeros(self.numStates)
                dNextState = self.applyAction(state, action)
                nextStates = self.getNeighbors(state)
                for nextState in nextStates:
                    if nextState == dNextState:
                        matrix[self.enumerated_state[nextState]] = 0.8
                    else:
                        matrix[self.enumerated_state[nextState]] = 0.2 / (len(nextStates) - 1)
                matrices.append(matrix)
            action_matrices.append(np.vstack(matrices))
        return action_matrices

    def getRewards(self):
        rewards = np.zeros(self.numStates)
        for goalState in self.goalStates:
            rewards[self.enumerated_state[goalState]] = 100
        action_rewards = [rewards for __ in range(len(ACTIONS))]
        return np.array(action_rewards).T


class QLearner:

    def __init__(self, model, epochs, learning_rate, epsilon, discount):
        self.model = model
        self.Q = {}
        self.epochs = epochs
        self.learning_rate = learning_rate
        self.epsilon = epsilon
        self.discount = discount
        self.decay = self.learning_rate / self.epochs

        self.initializeQ()

        self.policy = None

    def initializeQ(self):
        for state in self.model.getStates():
            self.Q[state] = {}
            for action in ACTIONS:
                self.Q[state][action] = 0

    def run(self):
        states = self.model.getStates()
        rewards = self.model.getRewards()

        epoch = 0
        while epoch < self.epochs:
            # print("Starting epoch: {}".format(epoch))
            state = states[np.random.randint(len(states))]
            while state not in self.model.goalStates:
                action = self.getAction(ACTIONS, state)
                next_state = self.model.applyAction(state, action)
                reward = self.getReward(rewards, next_state, states, action)
                self.updateQ(state, action, next_state, reward)
                state = next_state
            self.learning_rate *= (1. / (1. + self.decay * epoch))
            epoch += 1

        self.policy = self.getPolicy()

    def updateQ(self, state, action, next_state, reward):
        bellman_update = reward + self.discount * max([self.Q[next_state][a] for a in ACTIONS]) - self.Q[state][action]
        self.Q[state][action] += self.learning_rate * bellman_update

    def getAction(self, actions, state):
        if np.random.random() <= self.epsilon:
            return list(actions.keys())[np.random.randint(len(actions))]
        return self.getBestAction(state)

    def getBestAction(self, state):
        bestAction = None
        bestValue = 0
        for action in self.Q[state]:
            if self.Q[state][action] >= bestValue:
                bestAction = action
        if bestValue == 0:
            bestAction = list(ACTIONS.keys())[np.random.randint(len(ACTIONS))]
        return bestAction

    def getReward(self, rewards, state, states, action):
        i = states.index(state)
        j = list(ACTIONS.keys()).index(action)
        return rewards[i][j]

    def getPolicy(self):
        policy = []
        for state in self.model.getStates():
            values = [self.Q[state][action] for action in ACTIONS]
            policy.append(np.asscalar(np.argmax(values)))
        return policy

    def hasConverged(self):
        new_policy = np.array(self.getPolicy())
        converged = (self.policy == new_policy).all()
        self.policy = new_policy
        return converged

    @property
    def iter(self):
        return self.epochs


if __name__ == "__main__":
    length = 40
    width = 40
    model = GridModel(length, width, [(8,7)])
    # policy = PolicyIteration(model.getTransitionProbs(), model.getRewards(), discount=0.7)
    # policy.run()
    # print(np.array(policy.policy).reshape((length, width)))
    # print("PolicyIteration converged in {} iterations.".format(policy.iter))
    #
    # value = ValueIteration(model.getTransitionProbs(), model.getRewards(), discount=0.7)
    # value.run()
    # print(np.array(value.policy).reshape(length, width))
    # print("ValueIteration converged in {} iterations.".format(value.iter))

    qlearning = QLearner(model, epochs=200, learning_rate=0.75, epsilon=0.5, discount=0.7)
    qlearning.run()
    print(np.array(qlearning.policy).reshape(length, width))